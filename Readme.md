# Client and server

Running the client and server programs for the first time should produce config files with default settings

# Nginx

To run the websockets through nginx as a reverse proxy, add the following lines. Modify to suit your needs

```
location /ws {
        proxy_pass http://127.0.0.1:8123/ws;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
}

location /source {
        proxy_pass http://127.0.0.1:8123/source;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
}

location /canvas-test {
        proxy_pass http://127.0.0.1:8123/;
}
```