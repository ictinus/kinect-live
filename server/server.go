package main

import (
	"encoding/json"
	"flag"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/websocket"
)

type Config struct {
	ListenAddr string
	Password   string
}

var config Config
var clients = make(map[*websocket.Conn]bool)
var broadcastChannel = make(chan []byte)
var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func main() {
	configFile := flag.String("c", "server.cfg", "Location of the config file")
	flag.Parse()
	loadConfig(*configFile)

	fs := http.FileServer(http.Dir("../webClient"))
	http.Handle("/", fs)
	http.HandleFunc("/ws", handleConnections)
	http.HandleFunc("/source", handleSourceConnections)

	go startBroadcasting()

	log.Printf("Started server on %s", config.ListenAddr)

	if err := http.ListenAndServe(config.ListenAddr, nil); err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func loadConfig(configFile string) {
	file, err := os.Open(configFile)
	if os.IsNotExist(err) {
		log.Print("No config file")
		newFile, err := os.Create(configFile)
		if err != nil {
			log.Fatal(err)
		}
		newCfg, err := json.MarshalIndent(Config{ListenAddr: "localhost:8123", Password: "ChangeMe"}, "", "    ")
		if err != nil {
			log.Fatal(err)
		}
		_, err = newFile.Write(newCfg)
		if err != nil {
			log.Fatal(err)
		}
		log.Print("New config file created")
		os.Exit(0)
	}
	if err != nil {
		log.Fatal(err)
	}
	decoder := json.NewDecoder(file)
	if err := decoder.Decode(&config); err != nil {
		log.Fatal(err)
	}
	log.Println("Config file loaded")
}

func handleSourceConnections(w http.ResponseWriter, r *http.Request) {
	// Upgrade connection
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println("Source connection upgrade error: ", err)
		return
	}

	defer ws.Close()

	log.Println("Source connected")

	// Crapy auth, please replace later
	_, authMsg, err := ws.ReadMessage()
	if err != nil {
		log.Println("Source auth failed with error: ", err)
	}

	if string(authMsg) != config.Password {
		log.Println("Source auth inavlid password: ", err)
		return
	}

	for {
		_, msg, err := ws.ReadMessage()
		if err != nil {
			log.Println("Source rcv error: ", err)
			break
		}

		log.Printf("Received %d bytes\n", len(msg))

		if len(clients) > 0 {
			broadcastChannel <- msg
		}
	}
}

func handleConnections(w http.ResponseWriter, r *http.Request) {
	// Upgrade connection
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println("Client connection upgrade error: ", err)
		return
	}

	defer ws.Close()

	log.Println("Client connected")

	// Add to client map
	clients[ws] = true

	// Listen for client messages
	for {
		msgType, msg, err := ws.ReadMessage()
		if err != nil {
			log.Println("ws error: ", err)
			delete(clients, ws)
			break
		}
		log.Printf("Client type: %d, msg: %v", msgType, msg)
	}
}

func startBroadcasting() {
	for {
		msg := <-broadcastChannel
		for client := range clients {
			if err := client.WriteMessage(websocket.BinaryMessage, msg); err != nil {
				log.Println("ws send error: ", err)
			}
		}
	}
}
