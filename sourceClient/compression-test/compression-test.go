package main

import (
	"path/filepath"
	"io"
	// "bytes"
	"fmt"
	// "io"
	"compress/flate"
	"compress/gzip"
	"encoding/binary"
	"log"
	"os"
	"time"

	"github.com/gorilla/websocket"
	"github.com/velovix/go-freenect"
)

const (
	PASSWORD = "PleaseChangeMeThanks"
	HEIGHT   = 480
	WIDTH    = 640
	// Skip this many frames every capture
	SkipFrames = 50
	PixelSkip  = 2
	ServerURL  = "ws://localhost:8123/source"
)

var (
	ogDepth       []uint16
	skippedFrames int
	sendQueue     = make(chan [][]uint16, 20)
)

func onDepthFrame(device *freenect.Device, depth []uint16, timestamp uint32) {
	if skippedFrames < SkipFrames {
		skippedFrames++
		fmt.Println("Frame Skipped!")
		return
	}

	skippedFrames = 0

	go func() {
		fmt.Println(len(depth))
		fmt.Println("Frame gotten!")

		// depthData := depth

		// scaledDepthData := depth
		scaledDepthData := scaleDepthData(depth)

		// depthData := scaledDepthData
		// depthData := applyInlineSignal(scaledDepthData)
		data := applyDepthResolutionMapping(scaledDepthData)

		// data := depthDatatoByteArray(depthData)
		// data := dataToVLCByteArray(depthData)

		// depthData := detlaEncode(scaledDepthData)
		// data := deltaDatatoByteArray(depthData)
		// data := deltaToVLCByteArray(depthData)
		
		// depthData := deltaSmallEncode(data)
		// data = deltaSmallToVLCByteArray(depthData)

		// diff := getDepthDiff(depthData)
		// data := depthDifftoByteArray(diff)

		stampOfTime := time.Now().Unix()

		// Write the data to file
		rawFilename := fmt.Sprintf("kinect_frame-%d.data", stampOfTime)
		rawFile, err := os.Create(rawFilename)
		rawFile.Write(data)

		//Compress and write data to file
		filename := fmt.Sprintf("kinect_frame-%d.gz", stampOfTime)
		gzipFile, err := os.Create(filename)
		if err != nil {
			log.Fatal(err)
		}

		// flateCompress(gzipFile, data)
		// return

		zw := gzip.NewWriter(gzipFile)

		// var buffer bytes.Buffer
		// zw := gzip.NewWriter(&buffer)

		_, err = zw.Write(data)
		if err != nil {
			log.Fatal(err)
		}

		if err := zw.Close(); err != nil {
			log.Fatal(err)
		}

		return

		// frame := make([][]uint16, HEIGHT)

		// for y := 0; y < HEIGHT; y++ {
		// 	frame[y] = depth[y*WIDTH : (y+1)*WIDTH]
		// }
		// fmt.Println("Frame Converted!")
		// scaledHeight := HEIGHT / PixelSkip
		// scaledWidth := WIDTH / PixelSkip

		// scaledFrame := make([][]uint16, scaledHeight)

		// // Scale the frame down
		// for y := 0; y < scaledHeight; y++ {
		// 	scaledFrame[y] = make([]uint16, scaledWidth)

		// 	for x := 0; x < scaledWidth; x++ {
		// 		scaledFrame[y][x] = frame[y*PixelSkip][x*PixelSkip]
		// 	}
		// }

		// fmt.Println("Scaled to ", scaledHeight, " x ", scaledWidth)
		// sendQueue <- scaledFrame
		// fmt.Println("Frame queued!")
	}()
}

func networkSend() {
	fmt.Println("Connecting to websocket server")
	conn, _, err := websocket.DefaultDialer.Dial(ServerURL, nil)
	if err != nil {
		panic(err)
	}
	err = conn.WriteMessage(websocket.TextMessage, []byte(PASSWORD))
	if err != nil {
		fmt.Println("Auth error: ", err)
	}
	for img := range sendQueue {
		fmt.Println("Sending out frame!")
		err := conn.WriteJSON(img)
		if err != nil {
			panic(err)
		}
	}
}

func main() {

	files, _ := filepath.Glob("kinect_frame-*")
	for _, f := range files {
		if err := os.Remove(f); err != nil {
			panic(err)
		}
	}

	context, err := freenect.NewContext()
	if err != nil {
		panic(err)
	}

	defer context.Destroy()

	context.SetLogLevel(freenect.LogDebug)

	if cnt, _ := context.DeviceCount(); cnt == 0 {
		fmt.Println("could not find any devices")
		os.Exit(1)
	}

	kinect, err := context.OpenDevice(0)
	if err != nil {
		panic(err)
	}

	defer kinect.Destroy()

	kinect.SetDepthCallback(onDepthFrame)

	err = kinect.StartDepthStream(freenect.ResolutionMedium, freenect.DepthFormat11Bit)
	if err != nil {
		panic(err)
	}

	// go networkSend()

	for {
		err := context.ProcessEvents(0)
		if err != nil {
			panic(err)
		}
	}
}

func scaleDepthData(depth []uint16) []uint16 {

	depthData := make([]uint16, len(depth)/4)

	var adjustedLength = 0
	for i := 0; i < len(depth); i++ {
		if i%2 == 0 || (i/640)%2 == 0 {
			continue
		}
		depthData[adjustedLength] = depth[i]
		adjustedLength++
	}

	fmt.Printf("Adjusted depth length is %d\n", adjustedLength)

	return depthData
}

func depthDatatoByteArray(depth []uint16) []byte {
	data := make([]byte, len(depth)*2)

	for i := 0; i < len(depth); i++ {
		binary.LittleEndian.PutUint16(data[i*2:], depth[i])
		// binary.BigEndian.PutUint16(data[i*2:], depth[i])
	}

	return data
}

func dataToVLCByteArray(depth []uint16) []byte {
	data := make([]byte, len(depth)*binary.MaxVarintLen16)
	byteCount := 0

	for i := 0; i < len(depth); i++ {
		byteCount += binary.PutUvarint(data[byteCount:], uint64(depth[i]))
	}

	fmt.Printf("Bytes %d \n", byteCount)

	return data[0:byteCount]
}

func deltaDatatoByteArray(depth []int16) []byte {
	data := make([]byte, len(depth)*2)

	for i := 0; i < len(depth); i++ {
		binary.LittleEndian.PutUint16(data[i*2:], uint16(depth[i]))
		// binary.BigEndian.PutUint16(data[i*2:], uint16(depth[i]))
	}

	return data
}

func deltaToVLCByteArray(depth []int16) []byte {
	data := make([]byte, len(depth)*binary.MaxVarintLen16)

	byteCount := 0

	for i := 0; i < len(depth); i++ {
		byteCount += binary.PutVarint(data[byteCount:], int64(depth[i]))
	}

	fmt.Printf("Bytes %d \n", byteCount)

	return data[0:byteCount]
}

func deltaSmallToVLCByteArray(depth []int8) []byte {
	data := make([]byte, len(depth)*binary.MaxVarintLen16)

	byteCount := 0

	for i := 0; i < len(depth); i++ {
		byteCount += binary.PutVarint(data[byteCount:], int64(depth[i]))
	}

	fmt.Printf("Bytes %d \n", byteCount)

	return data[0:byteCount]
}



func applyInlineSignal(depth []uint16) []uint16 {

	

	processedDepth := make([]uint16, len(depth))
	copy(processedDepth, depth)

	if len(ogDepth) > 0 {
		samesiesCount := 0
		for i := 0; i < len(ogDepth); i++ {
			if processedDepth[i] == ogDepth[i] {
				processedDepth[i] = 2047
				samesiesCount++
			}
		}
		fmt.Printf("Samesies!!! %d \n", samesiesCount)
		// fmt.Printf("%v", processedDepth)
	} else {
		fmt.Println("Setting OG depth")
		ogDepth = make([]uint16, len(depth))
	}

	copy(ogDepth, depth)

	fmt.Printf("OG depth length %d \n", len(ogDepth))

	return processedDepth
}

func getDepthDiff(depth []uint16) map[uint32]uint16 {

	depthDiff := map[uint32]uint16{}

	if len(ogDepth) > 0 {
		samesiesCount := 0
		for i := 0; i < len(ogDepth); i++ {
			if depth[i] != ogDepth[i] {
				depthDiff[uint32(i)] = ogDepth[i]
				samesiesCount++
			}
		}
		fmt.Printf("Samesies!!! %d \n", samesiesCount)
	} else {
		fmt.Println("Setting OG depth")
		ogDepth = make([]uint16, len(depth))
	}

	copy(ogDepth, depth)

	fmt.Printf("OG depth length %d \n", len(ogDepth))

	return depthDiff
}

func depthDifftoByteArray(depth map[uint32]uint16) []byte {

	fmt.Printf("Diff length %d \n", len(depth))

	data := make([]byte, len(depth)*6)

	index := 0
	for k, v := range depth {
		binary.LittleEndian.PutUint32(data[index:], k)
		binary.LittleEndian.PutUint16(data[index+4:], v)
		index += 6
	}

	return data
}

func detlaEncode(depth []uint16) []int16 {

	deltaDepth := make([]int16, len(depth))

	for i := 0; i < len(depth); i++ {
		if i == 0 {
			deltaDepth[i] = int16(depth[i])
			continue
		}

		deltaDepth[i] = int16(depth[i-1] - depth[i])

	}

	return deltaDepth
}

func deltaSmallEncode(depth []uint8) []int8 {
	deltaDepth := make([]int8, len(depth))

	for i := 0; i < len(depth); i++ {
		if i == 0 {
			deltaDepth[i] = int8(depth[i])
			continue
		}

		deltaDepth[i] = int8(depth[i-1] - depth[i])

	}

	return deltaDepth
}

func flateCompress(w io.Writer, data []byte) {

	zw, _ := flate.NewWriter(w, flate.BestCompression)

	_, err := zw.Write(data)
	if err != nil {
		log.Fatal(err)
	}

	if err := zw.Close(); err != nil {
		log.Fatal(err)
	}
}

func applyDepthResolutionMapping(depth []uint16) []uint8 {
	mappedDepth := make([]uint8, len(depth))

	for k, v := range depth {


		mappedDepth[k] = uint8(255*(float32(v)/2047))
	}

	fmt.Printf("%v", mappedDepth)
	return mappedDepth
}
