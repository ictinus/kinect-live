package main

import (
	"bytes"
	"compress/gzip"
	"encoding/binary"
	"encoding/json"
	"flag"
	"io"
	"log"
	"os"
	"time"

	"github.com/gorilla/websocket"
	"github.com/velovix/go-freenect"
)

const (
	// SkipFrames represents the number of frames skipped every capture

	depthFrameWidth = 640
	depthFormat     = freenect.DepthFormatMM
	scaleFactor     = 2
)

type Config struct {
	URL       string
	Password  string
	FrameSkip int
}

var (
	config        Config
	frameCount    = 0
	byteRateCount = 0
	skippedFrames int
	sendQueue     = make(chan bytes.Buffer)
)

func main() {
	configFile := flag.String("c", "kinect.cfg", "Location of the config file")
	flag.Parse()
	loadConfig(*configFile)

	skippedFrames = config.FrameSkip

	context, err := freenect.NewContext()
	if err != nil {
		panic(err)
	}

	defer context.Destroy()

	context.SetLogLevel(freenect.LogDebug)

	if cnt, _ := context.DeviceCount(); cnt == 0 {
		log.Println("Could not find any devices")
		os.Exit(1)
	}

	kinect, err := context.OpenDevice(0)
	if err != nil {
		panic(err)
	}

	defer kinect.Destroy()

	kinect.SetDepthCallback(onDepthFrameReceived)

	if err := kinect.StartDepthStream(freenect.ResolutionMedium, depthFormat); err != nil {
		panic(err)
	}

	go startNetworkSend()

	go startRateTracking(5)

	// Process kinect data
	for {
		if err := context.ProcessEvents(0); err != nil {
			panic(err)
		}
	}

}

func loadConfig(configFile string) {
	file, err := os.Open(configFile)
	if os.IsNotExist(err) {
		log.Print("No config file")
		newFile, err := os.Create(configFile)
		if err != nil {
			log.Fatal(err)
		}
		newCfg, err := json.MarshalIndent(Config{URL: "ws://localhost:8123/source", Password: "ChangeMe", FrameSkip: 5}, "", "    ")
		if err != nil {
			log.Fatal(err)
		}
		_, err = newFile.Write(newCfg)
		if err != nil {
			log.Fatal(err)
		}
		log.Print("New config file created")
		os.Exit(0)
	}
	if err != nil {
		log.Fatal(err)
	}
	decoder := json.NewDecoder(file)
	if err := decoder.Decode(&config); err != nil {
		log.Fatal(err)
	}
	if config.URL == "" {
		log.Fatal("No URL in config")
	}
	if config.Password == "" {
		log.Println("Warning: config Password is empty")
	}
	if config.FrameSkip == 0 {
		log.Println("Warning: config FrameSkip is 0")
	}
	log.Println("Config file loaded")
}

func startNetworkSend() {

	log.Println("Connecting to websocket server")
	// Startup the websocket connection
	conn, _, err := websocket.DefaultDialer.Dial(config.URL, nil)
	if err != nil {
		log.Println("Websocket connection failed")
		panic(err)
	}

	// Send the auth message
	if err := conn.WriteMessage(websocket.TextMessage, []byte(config.Password)); err != nil {
		log.Println("Auth error: ", err)
		panic(err)
	}

	log.Println("Connected")

	for frame := range sendQueue {
		// Update byteRateCount for tracker
		byteRateCount += frame.Len()

		// Send frame over the websocket
		if err := conn.WriteMessage(websocket.BinaryMessage, frame.Bytes()); err != nil {
			panic(err)
		}
	}
}

// Frame and byte rate tracker
func startRateTracking(interval int) {

	duration := float64(interval)
	ticker := time.NewTicker(time.Duration(duration) * time.Second)
	defer ticker.Stop()

	for {
		<-ticker.C
		fps := float64(frameCount) / duration
		frameCount = 0
		log.Printf("FPS %f\n", fps)

		kbps := float64(byteRateCount) / duration / 1024
		byteRateCount = 0
		log.Printf("KBps %f\n", kbps)
	}
}

func onDepthFrameReceived(device *freenect.Device, depth []uint16, timestamp uint32) {

	if skippedFrames < config.FrameSkip {
		skippedFrames++
		return
	}

	skippedFrames = 0
	frameCount++

	go func() {

		scaledDepthData := scaleDepthData(depth)

		// scaledDepthData := depth

		data := applyDepthResolutionMapping(scaledDepthData)

		// data := depthDatatoByteArray(scaledDepthData)

		var buffer bytes.Buffer

		gzipCompress(&buffer, data)

		sendQueue <- buffer
	}()
}

func scaleDepthData(depth []uint16) []uint16 {

	depthData := make([]uint16, len(depth)/(scaleFactor*scaleFactor))

	var adjustedLength = 0
	for i := 0; i < len(depth); i++ {
		if i%scaleFactor != 0 || (i/depthFrameWidth)%scaleFactor != 0 {
			continue
		}
		depthData[adjustedLength] = depth[i]
		adjustedLength++
	}

	return depthData
}

func applyDepthResolutionMapping(depth []uint16) []uint8 {
	mappedDepth := make([]uint8, len(depth))

	for k, v := range depth {

		switch depthFormat {
		// 10 bit frame depth
		case freenect.DepthFormat10Bit:
			mappedDepth[k] = uint8(255 * (float32(v) / 2047))
		// 11 bit frame depth
		case freenect.DepthFormat11Bit:
			mappedDepth[k] = uint8(255 * (float32(v) / 2047))
		// Millimeter frame depth
		case freenect.DepthFormatMM:
			var maxDepth uint16 = 8192
			// Clamp to max depth
			if v == 0 || v > maxDepth {
				v = maxDepth
			}
			mappedDepth[k] = uint8(255 * (float32(v) / float32(maxDepth)))
		default:
			panic("Unsupported depth format for depth resolution mapping.")
		}

	}

	return mappedDepth
}

func gzipCompress(w io.Writer, data []byte) {

	zw, _ := gzip.NewWriterLevel(w, gzip.DefaultCompression)

	_, err := zw.Write(data)
	if err != nil {
		panic(err)
	}

	if err := zw.Close(); err != nil {
		panic(err)
	}
}

func depthDatatoByteArray(depth []uint16) []byte {
	data := make([]byte, len(depth)*2)

	for i := 0; i < len(depth); i++ {
		binary.LittleEndian.PutUint16(data[i*2:], depth[i])
		// binary.BigEndian.PutUint16(data[i*2:], depth[i])
	}

	return data
}
